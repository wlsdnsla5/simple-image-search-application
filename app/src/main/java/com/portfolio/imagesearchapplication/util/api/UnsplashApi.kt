package com.portfolio.imagesearchapplication.util.api


import com.portfolio.imagesearchapplication.BuildConfig
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

/*
* "BuildConfig.API_KEY" can produce error
* Simple solution :
*   1. File -> Invalidate Caches/Restart... -> Invalidate and Restart
*   2. Build -> Clean Project
*   3. Build -> Rebuild Project
*/
interface UnsplashApi {

    companion object {
        const val CLIENT_ID = BuildConfig.API_KEY
        const val BASE_URL = "https://api.unsplash.com/"
    }

    @Headers("Accept-Version: v1","Authorization: Client-ID $CLIENT_ID")
    @GET("search/photos")
    suspend fun searchPhotos(
        @Query("query") query: String,
        @Query("page") page: Int,
        @Query("per_page") perPage: Int
    ) : UnsplashResponse
}