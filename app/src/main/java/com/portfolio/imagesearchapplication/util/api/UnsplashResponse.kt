package com.portfolio.imagesearchapplication.util.api

import com.portfolio.imagesearchapplication.data.photo.UnsplashPhoto

data class UnsplashResponse(
    val results: List<UnsplashPhoto>
)