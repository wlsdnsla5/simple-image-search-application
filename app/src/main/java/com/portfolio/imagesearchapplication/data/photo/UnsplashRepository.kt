package com.portfolio.imagesearchapplication.data.photo

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.liveData
import com.portfolio.imagesearchapplication.util.api.UnsplashApi
import javax.inject.Inject
import javax.inject.Singleton


/*
* @Singleton is for instance which will be created once in lifecycle
* */
@Singleton
class UnsplashRepository @Inject constructor(private val api: UnsplashApi) {
    fun getSearchResults(query: String) = Pager(
        config = PagingConfig(
            pageSize = 20,
            maxSize = 100,
            enablePlaceholders = false
        ),
        pagingSourceFactory = {
            UnsplashPagingSource(api, query)
        }
    ).liveData
}