# simple image search application

unsplash api - Open Photo Api : https://unsplash.com/developers, Android Paging 3 - for pagination, Glide - image load library, Retrofit2, Coroutines , Navigation Component, View Binding, Dagger2, Room library, RxJava - for concurrency,  LiveData 

This application is example of simple image search application. which is using technologies which above. 

Expected action : 
    Search image
    Like other photo
    View search history 
    View like history

Functionality can be extended later.
